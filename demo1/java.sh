#!/usr/bin/env bash

sudo apt-get update --fix-missing -y && sudo apt-get install openjdk-14-jdk -y

function clone_pull {
    Dir=$(basename "$1" .git)
    if [[ -d "$Dir" ]]; then
      cd $Dir
      git pull
    else
      git clone "$1" && cd $Dir
    fi
}

clone_pull https://gitlab.com/Said_Abilov/demo1.git

sudo chmod +x mvnw

./mvnw clean package
